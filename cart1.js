var fruit = [
    {
        "name":"Apple",
        "category":"Fruit",
        "price":100,
        "quantity":0,

    },
    {
        "name":"Mango",
        "category":"Fruit",
        "price":50,
        "quantity":0,

    },
    {
        "name":"Banana",
        "category":"Fruit",
        "price":30,
        "quantity":0,

    },
    {
        "name":"Papaya",
        "category":"Fruit",
        "price":35,
        "quantity":0,

    },
    {
        "name":"Grapes",
        "category":"Fruit",
        "price":45,
        "quantity":0,

    },
    {
        "name":"Pomegranate",
        "category":"Fruit",
        "price":50,
        "quantity":0,

    },
    {
        "name":"Plum",
        "category":"Fruit",
        "price":30,
        "quantity":0,

    },
    {
        "name":"Cherry",
        "category":"Fruit",
        "price":35,
        "quantity":0,

    },
    {
        "name":"Guava",
        "category":"Fruit",
        "price":40,
        "quantity":0,

    },
    {
        "name":"Melon",
        "category":"Fruit",
        "price":50,
        "quantity":0,

    },
];

var vegetables = [
    {
        "name":"Ladyfinger",
        "category":"Vegetable",
        "price":100,
        "quantity":0,

    },
    {
        "name":"Potato",
        "category":"Vegetable",
        "price":50,
        "quantity":0,

    },
    {
        "name":"Tomato",
        "category":"Vegetable",
        "price":30,
        "quantity":0,

    },
    {
        "name":"Onion",
        "category":"Vegetable",
        "price":35,
        "quantity":0,

    },
    {
        "name":"Garlic",
        "category":"Vegetable",
        "price":45,
        "quantity":0,

    },
    {
        "name":"Ginger",
        "category":"Vegetable",
        "price":50,
        "quantity":0,

    },
    {
        "name":"Cauliflower",
        "category":"Vegetable",
        "price":30,
        "quantity":0,

    },
    {
        "name":"Lettuce",
        "category":"Vegetable",
        "price":35,
        "quantity":0,

    },
    {
        "name":"Brocolli",
        "category":"Vegetable",
        "price":40,
        "quantity":0,

    },
    {
        "name":"Capsicum",
        "category":"Vegetable",
        "price":50,
        "quantity":0,

    },
];

var dairy = [
    {
        "name":"Tofu",
        "category": "dairy",
        "price": 80,
        "quantity":0,
    },
    {
        "name":"Cottage Cheeze",
        "category": "dairy",
        "price": 70,
        "quantity":0,

    },
    {
        "name":"Milk",
        "category": "dairy",
        "price": 30,
        "quantity":0,
    },
    {
        "name":"Egg",
        "category": "dairy",
        "price": 4,
        "quantity":0,
    },
    {
        "name":"Chicken",
        "category": "dairy",
        "price": 250,
        "quantity":0,
    },
    {
        "name":"Beef",
        "category": "dairy",
        "price": 230,
        "quantity":0,
    },
    {
        "name":"Butter",
        "category": "dairy",
        "price": 50,
        "quantity":0,
    },
    {
        "name":"Cheeze",
        "category": "dairy",
        "price": 60,
        "quantity":0,
    },
    {
        "name":"Curd",
        "category": "dairy",
        "price": 40,
        "quantity":0,
    },
    {
        "name":"Yogurt",
        "category": "dairy",
        "price": 30,
        "quantity":0,
    },
    
];

var dryfruit = [
    {
        "name":"Walnut",
        "category": "dryfruit",
        "price": 400,
        "quantity":0,
    },
    {
        "name":"Almond",
        "category": "dryfruit",
        "price": 300,
        "quantity":0,
    },
    {
        "name":"Fennel",
        "category": "dryfruit",
        "price": 50,
        "quantity":0,
    },
    {
        "name":"Flax Seed",
        "category": "dryfruit",
        "price": 200,
        "quantity":0,
    },
    {
        "name":"Cashew",
        "category": "dryfruit",
        "price": 250,
        "quantity":0,
    },
    {
        "name":"Coconut",
        "category": "dryfruit",
        "price": 100,
        "quantity":0,
    },
    {
        "name":"Raisin",
        "category": "dryfruit",
        "price": 250,
        "quantity":0,
    },
    {
        "name":"Dates",
        "category": "dryfruit",
        "price": 250,
        "quantity":0,
    },
    {
        "name":"Peanut",
        "category": "dryfruit",
        "price": 100,
        "quantity":0,
    },
    {
        "name":"Lotus Seed",
        "category": "dryfruit",
        "price": 50,
        "quantity":0,
    },
    {
        "name":"Pistachio",
        "category": "dryfruit",
        "price": 150,
        "quantity":0,
    }
];

var results = [];


function dataSearch(){
   
    var item = document.getElementById("searchbar").value;
    fruit.forEach((obj) => {
            if(obj.name.toLowerCase().includes(item)){
                console.log("hi")
              
                var row = tab.insertRow(-1);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                
                cell1.innerHTML = obj.name;
                cell2.innerHTML = obj.category;
                cell3.innerHTML = obj.price;
                cell4.innerHTML =  '<input id="Quan" type="number" value="0" />';
                obj.quantity = document.getElementById("Quan").value;            }
        });    
        vegetables.forEach((obj) => {
            if(obj.name.toLowerCase().includes(item)){
                console.log("hi")
                
                var row = tab.insertRow(-1);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                
                cell1.innerHTML = obj.name;
                cell2.innerHTML = obj.category;
                cell3.innerHTML = obj.price;
                cell4.innerHTML =  '<input id="Quan" type="number" value="0" />';
                obj.quantity = document.getElementById("Quan").value;
            }
        });
        dairy.forEach((obj) => {
            if(obj.name.toLowerCase().includes(item)){
                console.log("hi")
               
                var row = tab.insertRow(-1);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                
                cell1.innerHTML = obj.name;
                cell2.innerHTML = obj.category;
                cell3.innerHTML = obj.price;
                cell4.innerHTML =  '<input id="Quan" type="number" value="0" />';
                obj.quantity = document.getElementById("Quan").value;
            }
        });    
        dryfruit.forEach((obj) => {
            if(obj.name.toLowerCase().includes(item)){
                console.log("hi")
                
                var row = tab.insertRow(-1);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                
                cell1.innerHTML = obj.name;
                cell2.innerHTML = obj.category;
                cell3.innerHTML = obj.price;
                cell4.innerHTML =  '<input id="Quan" type="number" value="0" />';
                obj.quantity = document.getElementById("Quan").value;
            }
        });    
    
}

function reset(){
    var tab1 = document.getElementById("tab");
    var rowCount = tab1.rows.length;
    for(var i=rowCount-1;i>0;i--){
        tab1.deleteRow(i);
    }
}

function quantityChange(){

}

function addItem(){
    var tab1 = document.getElementById("tab");
    var totalRows = tab1.rows.length;
    var itemsArray=[];
    var quantityArray=[];
    var rowCount = totalRows-1;

    console.log(rowCount);
    for( var i = 1 ;i < rowCount + 1; i++ ){
        console.log("in");
        let itemQuantity = document.getElementById("Quan").value;
        if(itemQuantity > 0)
        itemsArray[i] = document.getElementById("tab").rows[i].cells[0].innerHTML;
    //    quantityArray[i] = document.getElementById("tab").rows[i].cells[4].innerHTML;
    }
    console.log(itemsArray);
   // console.log(quantityArray);

    // fruit.forEach((obj) => {
    //     if(obj.quantity > 0){
    //         console.log("yes")
    //         itemsArray.push(obj);
    //         }
    // });    
    //     console.log(itemsArray);
}  